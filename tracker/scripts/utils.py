import requests

# import lxml
from bs4 import BeautifulSoup

# Code refrenced https://blog.pyplane.com/blog/amazon-price-tracker-in-django/


HEADERS = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit"
    "/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36",
    "Accept-Language": "en-GB,en-US;q=0.9,en;q=0.8",
}


def test_function(product):
    print("HELLO FROM ANOTHER FILE")
    print(product.name)
    print(product.owner)
    print(product.current_price)
    print(product.reference_url)


def get_product_data(product):
    url = product.reference_url
    response = requests.get(url, headers=HEADERS)

    soup = BeautifulSoup(response.text, "lxml")
    img_src = find_img(soup)
    price = find_price(soup)

    return price, img_src


def find_img(soup):
    img_div = soup.select_one(selector="#landingImage")
    if img_div:
        img_src = img_div.get("src")
    else:
        img_div = soup.select_one(selector="#imgBlkFront")
    if img_div:
        img_src = img_div.get("src")
    else:
        img_src = ""
    return img_src


def find_price(soup):
    price = soup.select_one(selector=".apexPriceToPay > span:nth-child(2)")
    if not price:
        price = soup.select_one(
            selector=".a-button-selected > span:nth-child(1) > a:nth-child(1)"
            " > span:nth-child(3) > span:nth-child(1)"
        )
    if not price:
        price = soup.select_one(
            selector=".reinventPricePriceToPayMargin > span:nth-child(1)"
        )

    price = price.getText()[1:].strip("$")
    price = float(price)
    return price


def update_price_data(product):
    url = product.reference_url
    response = requests.get(url, headers=HEADERS)

    soup = BeautifulSoup(response.text, "lxml")
    price = find_price(soup)
    return price
