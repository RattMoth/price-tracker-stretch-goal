# Generated by Django 4.0.5 on 2022-06-29 18:17

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tracker", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="product",
            name="created",
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name="product",
            name="updated",
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
