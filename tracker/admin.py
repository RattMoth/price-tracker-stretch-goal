from django.contrib import admin
from tracker.models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass
