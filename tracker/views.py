from django.views.generic import CreateView, ListView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy

from tracker.models import Product
from tracker.scripts.utils import update_price_data, get_product_data


class ProductListView(LoginRequiredMixin, ListView):
    model = Product
    template_name = "products/list.html"

    def get_queryset(self):
        return Product.objects.filter(owner=self.request.user)


@login_required()
def update_product(request, pk):
    user = request.user

    product = get_object_or_404(Product, pk=pk, owner=user)

    if request.method == "POST":
        new_price = update_price_data(product)
        Product.objects.filter(pk=pk).update(current_price=new_price)
        return redirect("home")


class ProductCreateView(LoginRequiredMixin, CreateView):
    model = Product
    template_name = "products/create.html"
    fields = ["name", "reference_url"]

    def form_valid(self, form):
        item = form.save(commit=False)

        try:
            price, picture_url = get_product_data(item)

            item.owner = self.request.user
            item.initial_price = price
            item.current_price = price
            item.picture_url = picture_url

            item.save()
            return redirect("home")
        except AttributeError:
            return self.form_invalid(form)

    def form_invalid(self, form):
        messages.warning(
            self.request,
            "Invalid URL."
            "Please double check that it links directly to a product.",
        )
        return self.render_to_response(
            self.get_context_data(request=self.request, form=form)
        )


class ProductDeleteView(LoginRequiredMixin, DeleteView):
    model = Product
    success_url = reverse_lazy("home")
