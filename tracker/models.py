from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError

USER_MODEL = settings.AUTH_USER_MODEL


def amazon_only_validator(value):
    if "amazon.com" in value:
        return value
    else:
        raise ValidationError(
            "Right now this only works for products on Amazon."
        )


class Product(models.Model):
    name = models.CharField(max_length=100)
    initial_price = models.DecimalField(max_digits=8, decimal_places=2)
    current_price = models.DecimalField(max_digits=8, decimal_places=2)
    reference_url = models.URLField(
        max_length=200,
        validators=[amazon_only_validator],
    )
    owner = models.ForeignKey(
        USER_MODEL, related_name="product", on_delete=models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    picture_url = models.URLField(blank=True)

    def __str__(self):
        return self.name
