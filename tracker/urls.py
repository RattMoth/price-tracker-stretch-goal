from django.urls import path

from tracker.views import (
    ProductCreateView,
    ProductDeleteView,
    ProductListView,
    update_product,
)

urlpatterns = [
    path("", ProductListView.as_view(), name="list_products"),
    path("create/", ProductCreateView.as_view(), name="create_products"),
    path("<int:pk>/", update_product, name="update_product"),
    path(
        "<int:pk>/delete", ProductDeleteView.as_view(), name="delete_product"
    ),
]
