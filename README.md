This is a simple Amazon price tracking site. It's still pretty janky, and Amazon is very inconsistent with how they display prices on
their site, so don't expect all links to work. However it _should_ work for most!

Please feel free to clone and play around with it.

Check out the demo here: https://streamable.com/b4xj6a
